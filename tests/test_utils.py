# from nysmix.utils import get_name_field_gen_mw
# from nysmix.schema import Snapshot
# from pandas import DataFrame


# def test_get_name_field_gen_mw() -> None:
#     assert get_name_field_gen_mw(
#         DataFrame(
#             [
#                 {
#                     "Time Stamp": "2002-12-12 01:01:01",
#                     "Time Zone": "EST",
#                     "Fuel Category": "Hydro",
#                     "Gen MW": 0.0,
#                 }
#             ]
#         )
#     ) in {Snapshot.gen_mw, Snapshot.gen_mwh}
