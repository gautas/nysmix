from pytest import fixture, mark, param
from sqlmodel import inspect, Session, text, SQLModel
from sqlalchemy.orm import close_all_sessions
from nysmix.concepts import Fuel, Timezone
from nysmix.db import Database, DatabaseInMemory, DatabasePostgres, File, Snapshot
from datetime import datetime, timedelta

from nysmix.settings import SettingsPostgres

NAME = "test"
LAST_MODIFIED = datetime(2000, 1, 1, 0, 0, 0, 0)
FILE = File(name=NAME, last_modified=LAST_MODIFIED)


@fixture(scope="module")
def db_memory() -> DatabaseInMemory:
    return DatabaseInMemory()


@fixture(scope="module")
def db_postgres() -> DatabasePostgres:
    db = DatabasePostgres()
    with db.engine.connect() as conn:
        conn.execute(text("COMMIT"))
        conn.execute(text("DROP DATABASE IF EXISTS testnysmix"))
        conn.execute(text("CREATE DATABASE testnysmix"))
    return DatabasePostgres(settings=SettingsPostgres(db="testnysmix"))


@fixture(
    scope="module", params=["db_memory", param("db_postgres", marks=mark.postgres)]
)
def db(request) -> Database:
    db = request.getfixturevalue(request.param)
    db.create()
    db.add_file(FILE)
    db.add_snapshots(
        [
            Snapshot(
                timestamp=datetime(2000, 1, 1, 0, 0, 0, 0),
                timezone=Timezone.EST,
                fuel=Fuel.DUAL_FUEL,
                gen_mw=10.0 + i,
            )
            for i in range(100)
        ]
    )
    return db


def test_create(db: Database) -> None:
    assert inspect(db.engine).has_table("file")


def test_add_file(db: Database) -> None:
    with Session(db.engine) as session:
        num_rows = session.query(File).count()
    assert num_rows == 1


def test_has_file_true(db: Database) -> None:
    assert db.has_file(FILE)


def test_has_file_false(db: Database) -> None:
    file1 = File(name=NAME + "test", last_modified=LAST_MODIFIED)
    file2 = File(name=NAME, last_modified=LAST_MODIFIED + timedelta(days=1))
    assert not db.has_file(file1)
    assert not db.has_file(file2)


def test_rm_file(db: Database) -> None:
    file1 = File(name=NAME + "test", last_modified=LAST_MODIFIED)
    assert not db.has_file(file1)
    db.add_file(file1)
    assert db.has_file(file1)
    db.rm_file(file1)
    assert not db.has_file(file1)


def test_add_snapshots(db: Database) -> None:
    with Session(db.engine) as session:
        num_rows = session.query(Snapshot).count()
    assert num_rows == 100
