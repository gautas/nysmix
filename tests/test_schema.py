from pandas import DataFrame
from nysmix.schema import Snapshot
from pytest import raises
from datetime import date
from nysmix.concepts import Timezone, Fuel


def test_snapshot_pass() -> None:
    try:
        Snapshot.validate(
            DataFrame(
                [
                    {
                        "Time Stamp": "2002-12-12 01:01:01",
                        "Time Zone": Timezone.EST,
                        "Fuel Category": Fuel.HYDRO,
                        "Gen MW": 0.0,
                    }
                ]
            )
        )
    except Exception as e:
        assert False, f"Exception raised: {e}"


def test_snapshot_fail() -> None:
    with raises(Exception):
        Snapshot.validate(
            DataFrame(
                [
                    {
                        "Time Stamp": "2002-12-12 02:02:02",
                        "Time Zone": Timezone.EST,
                        "Fuel Category": Fuel.HYDRO,
                        "Gen MWh": -1.0,
                    }
                ]
            )
        )


# def test_summary_pass() -> None:
#     try:
#         Summary.validate(
#             DataFrame(
#                 [
#                     {
#                         "date": date(2002, 12, 12),
#                         "Fuel Category": "Hydro",
#                         "gen_mw": 0.0,
#                     }
#                 ]
#             )
#         )
#     except Exception as e:
#         assert False, f"Exception raised: {e}"

# def test_summary_fail() -> None:
#     with raises(Exception):
#         Summary.validate(
#             DataFrame(
#                 [
#                     {
#                         "date": date(2002, 12, 12),
#                         "Fuel Category": "Steam",
#                         "gen_mw": -1.0,
#                     }
#                 ]
#             )
#         )
