from collections.abc import Iterator
from datetime import datetime, timedelta

from pytest import fixture, mark, param

from nysmix.repository import Repository, RepositoryGCS, RepositoryInMemory

LAST_MODIFIED = datetime(2000, 1, 1, 0, 0, 0)


@fixture(
    scope="module",
    params=[
        {"repo": RepositoryInMemory, "repo_args": {}},
        param(
            {"repo": RepositoryGCS, "repo_args": {"prefix": "test_"}}, marks=mark.google
        ),
    ],
)
def repo(request) -> Iterator[Repository]:
    repo = request.param["repo"](**request.param["repo_args"])
    repo.add(name="test", last_modified=LAST_MODIFIED, contents=b"test")
    repo.add(name="test", last_modified=LAST_MODIFIED, contents=b"test diff")
    repo.add(
        name="test",
        last_modified=LAST_MODIFIED + timedelta(seconds=1),
        contents=b"test test",
    )
    repo.add(name="test2", last_modified=LAST_MODIFIED, contents=b"test2")
    yield repo
    repo.clear()


@mark.parametrize(["name", "expected"], [("test", True), ("other", False)])
def test_contains(repo: Repository, name: str, expected: bool) -> None:
    assert repo.contains(name) is expected


@mark.parametrize(
    ["name", "expected"],
    [
        ("test", {LAST_MODIFIED, LAST_MODIFIED + timedelta(seconds=1)}),
        ("test2", {LAST_MODIFIED}),
    ],
)
def test_get_last_modifieds(
    repo: Repository, name: str, expected: set[datetime]
) -> None:
    assert repo.get_last_modifieds(name) == expected


@mark.parametrize(
    ["name", "last_modified", "expected"],
    [
        ("test", LAST_MODIFIED, b"test diff"),
        ("test", LAST_MODIFIED + timedelta(seconds=1), b"test test"),
        ("test2", LAST_MODIFIED, b"test2"),
    ],
)
def test_get(
    repo: Repository,
    name: str,
    last_modified: datetime,
    expected: bytes,
) -> None:
    assert repo.get(name=name, last_modified=last_modified) == expected
