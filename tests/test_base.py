from nysmix.base import TimeUnitBase, DayBase, MonthBase
from nysmix.config import TZ
from datetime import datetime, timedelta
from pytest import mark


@mark.parametrize(
    ["time_unit", "expected"],
    [
        (MonthBase(year=2020, month=6), False),
        (MonthBase(year=2015, month=12), False),
        (MonthBase(year=2015, month=11), True),
        (DayBase(year=2015, month=12, day=9), False),
        (DayBase(year=2015, month=12, day=8), True),
    ],
)
def test_is_before_start(time_unit: TimeUnitBase, expected: bool) -> None:
    assert time_unit.is_before_start == expected


@mark.parametrize(
    ["time_unit", "expected"],
    [
        (MonthBase(year=2020, month=6), False),
        (MonthBase.from_date(dt=datetime.now(TZ).date()), False),
        (MonthBase.from_date(dt=(datetime.now(TZ) + timedelta(days=40)).date()), True),
        (DayBase(year=2015, month=12, day=9), False),
        (DayBase.from_date(dt=(datetime.now(TZ) + timedelta(days=1)).date()), True),
    ],
)
def test_is_future(time_unit: TimeUnitBase, expected: bool) -> None:
    assert time_unit.is_future == expected
