from nysmix.data import All, Year, Month
from nysmix.base import MonthBase
from datetime import date


def test_all_years():
    all = All()
    assert Year(repo=all.repo, db=all.db, year=2018) in all.years


def test_year_months():
    year = Year(year=2018)
    assert (
        Month(
            repo=year.repo, db=year.db, month_base=MonthBase.from_date(date(2018, 5, 5))
        )
        in year.months
    )
