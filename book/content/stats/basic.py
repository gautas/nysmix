# # %% [markdown]
# """
# # Basic stats
# """

# # %% tags=['hide-cell']
# from IPython import get_ipython

# if get_ipython() is not None:
#     get_ipython().run_line_magic("load_ext", "autoreload")
#     get_ipython().run_line_magic("autoreload", "2")

# from datetime import timedelta

# import altair as alt
# from myst_nb import glue
# from pandas import DataFrame, to_datetime

# from nysmix.concepts import MEMBERS, NAME, SimpleFuel

# # %%
# from nysmix.data import All
# from nysmix.settings import SettingsGCS


# # %%
# df = All(memory=memory).summary

# # %%
# df.dtypes

# # %%
# df["date"] = to_datetime(df["date"])

# # %%
# df["date"].min(), df["date"].max()

# # %%
# df.head()

# # %%
# df_week = (
#     df.set_index("date")
#     .groupby(["Fuel Category"])["gen_mw"]
#     .resample("W")
#     .sum()
#     .reset_index()
# )

# # %%
# s_rolling = (
#     df_week.groupby(["date", "Fuel Category"], as_index=False)
#     .agg({"gen_mw": "sum"})
#     .sort_values("date")
#     .groupby(["Fuel Category"], as_index=False)
#     .rolling(window="365D", on="date")["gen_mw"]  # 365D
#     .sum()
# )

# # %%
# s_rolling_pcnt = s_rolling / s_rolling.groupby(["date"]).transform("sum")
# # %%
# df_rolling_pcnt = DataFrame(s_rolling_pcnt).reset_index()

# # %%
# len(df_rolling_pcnt)

# # %%
# df["Fuel Category"].value_counts()

# # %%
# alt.Chart(df_rolling_pcnt).mark_line().encode(
#     x="date", y=alt.Y("gen_mw", axis=alt.Axis(format="%")), color="Fuel Category"
# ).interactive()

# # %%
# def get_simple_fuel(fuel: str) -> str:
#     for simple in SimpleFuel:
#         for member in MEMBERS[simple]:
#             if fuel == NAME[member]:
#                 return NAME[simple]
#     return "Not found"


# # %%
# df_rolling_pcnt["Fuel Category Group"] = df_rolling_pcnt["Fuel Category"].map(
#     get_simple_fuel
# )

# # %%
# df_rolling_pcnt_group = df_rolling_pcnt.groupby(
#     ["date", "Fuel Category Group"], as_index=False
# ).agg({"gen_mw": "sum"})

# # %%
# nearest = alt.selection(
#     type="single", nearest=True, on="mouseover", fields=["date"], empty="none"
# )

# # %%
# selectors = (
#     alt.Chart(df_rolling_pcnt_group)
#     .mark_point()
#     .encode(
#         x="date",
#         opacity=alt.value(0),
#     )
#     .add_selection(nearest)
# )

# # %%
# line = (
#     alt.Chart(df_rolling_pcnt_group)
#     .mark_line()
#     .encode(
#         x="date",
#         y=alt.Y("gen_mw", axis=alt.Axis(format="%")),
#         color="Fuel Category Group",
#         tooltip="date",
#     )
# )

# # %%
# points = line.mark_point().encode(
#     opacity=alt.condition(nearest, alt.value(1), alt.value(0))
# )

# # Draw text labels near the points, and highlight based on selection
# text = line.mark_text(align="left", dx=5, dy=-5).encode(
#     text=alt.condition(nearest, alt.Text("gen_mw:Q", format=".0%"), alt.value(" "))
# )

# # Draw a rule at the location of the selection
# rules = (
#     alt.Chart(df_rolling_pcnt_group)
#     .mark_rule(color="gray")
#     .encode(
#         x="date",
#     )
#     .transform_filter(nearest)
# )

# # %%
# glue(
#     "chart",
#     alt.layer(line, selectors, points, rules, text)
#     .properties(
#         title="12 month rolling average proportion of NYS power generation by fuel"
#     )
#     .interactive(),
# )
# # %%
