# New York State fuel mix

```{glue:figure} chart
```

## Data
NYISO real-time fuel mix data lives here http://mis.nyiso.com/public/P-63list.htm

As far as I understand, these files are updated every 5 minutes. The first (top) section has links for CSV files for each of the days of the current month (including the current day). The second (bottom) section titled "Archived Files (zip format)" has links to zip files for each month of data (including the current month) going back to December 2015. We focus on these zip files, as the current month's zip file is expected to have all of the data in the top section daily CSV files.

## How it works
### File repository
Monthly zips and extracted daily csvs are stored to a file repository. You can use the in-memory (`nysmix.repository.RepositoryInMemory`) and Google Cloud Storage (`nysmix.repository.RepositoryGCS`) implementations or implement your own subclass of the abstraction `nysmix.repository.Repository`.

### Database
Daily csvs are loaded into a database. You can use the in-memory (`nysmix.db.DatabaseInMemory`) and Google BigQuery (`nysmix.db.DatabaseBigQuery`) implementations or implement your own subclass of the abstraction `nysmix.db.Database`.

### Data pipeline
#### Step 1: pull zip files
- Pull monthly data zip files to a filesystem of choice if the zip file has not yet been pulled or the modified date of the pulled zip file is out-of-date.

```py
from nysmix.data import All
All(repo=repo).pull()
```
#### Step 2: extract csvs
- Extract the daily csvs from newly pulled monthly zip files.

```py
from nysmix.data import All
All(repo=repo).extract()
```

#### Step 3: load into db
- Standardize schema and load into database of choice

```py
from nysmix.data import All
All(repo=repo, db=db).load()
```

### Serve via API
- daily, monthly, yearly summaries of generation by fuel
- 12 month rolling average proportion of generation by fuel
    - by day and month