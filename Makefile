.PHONY: dev_image dev_container book serve_book pull extract load test test_all bump

dev_image:
	docker build -t nysmix -f dev.dockerfile .

dev_container:
	docker run -it --rm -d --name nysmix --env-file .env -v $(shell pwd):/nysmix nysmix

book:
	poetry run jb build book

serve:
	python -m http.server -d book/_build/html 8000

pull:
	poetry run python scripts/pull.py

extract:
	poetry run python scripts/extract.py

load:
	poetry run python scripts/load.py

test:
	poetry run pytest -m "not google and not postgres"

test_google:
	poetry run pytest -v -m "google"

test_postgres:
	poetry run pytest -v -m "postgres"

bump:
	poetry version minor