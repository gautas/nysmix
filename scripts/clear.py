"""
clear the repository
"""

from logging import INFO, basicConfig
from sys import stdout

from nysmix.repository import RepositoryGCS

basicConfig(stream=stdout, level=INFO)

repo = RepositoryGCS()
repo.clear()
