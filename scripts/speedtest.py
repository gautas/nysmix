"""
pandas vs pyarrow speed test: iteratively building up a dataset
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from nysmix.data import Year
from nysmix.repository import RepositoryGCS
from datetime import datetime
from pandas import read_csv, concat
from io import BytesIO
from nysmix.schema import Snapshot
from pyarrow import fs, concat_tables, Table
from nysmix.settings import SettingsGCS
from pyarrow.parquet import read_table, write_table

# %%
repo = RepositoryGCS(prefix="test_")

# %%
yr = Year(repo=repo, year=2020)
lm = datetime.now()

# %%
# yr.pull()

# %%
# yr.extract()

# %%
days = [day for month in yr.months for day in month.days]

# %%
# for day in days:
#     print(day.day_base)
#     df_day = day.df
#     if not repo.contains(name="test.csv"):
#         df = df_day
#     else:
#         df = read_csv(BytesIO(repo.get(name="test.csv", last_modified=lm)))
#     df_day["year"] = df_day[Snapshot.timestamp].dt.year
#     df_day["month"] = df_day[Snapshot.timestamp].dt.month
#     df = concat([df, df_day])
#     with BytesIO() as f:
#         df.to_csv(f, index=False)
#         f.seek(0)
#         contents = f.read()
#     repo.add(name="test.csv", last_modified=lm, contents=contents)

# %%
gcs = fs.GcsFileSystem()

# %%


# %%
file_list = gcs.get_file_info(
    fs.FileSelector(SettingsGCS().name_bucket, recursive=True)
)

# %%
fi = file_list[0]
# %%
fi.path
# %%
yr.months[0].days[0].day_base.name_file_csv
# %%
for day in days:
    print(day.day_base)
    df_day = day.df
    df_day[Snapshot.timezone] = df_day[Snapshot.timezone].astype(str)
    df_day[Snapshot.fuel] = df_day[Snapshot.fuel].astype(str)
    df_day["year"] = df_day[Snapshot.timestamp].dt.year
    df_day["month"] = df_day[Snapshot.timestamp].dt.month
    if not repo.contains(name="test.parquet"):
        df = Table.from_pandas(df_day)
    else:
        df = read_table(f"{SettingsGCS().name_bucket}/test.parquet", filesystem=gcs)
        df = concat_tables([df, Table.from_pandas(df_day)])
    write_table(df, f"{SettingsGCS().name_bucket}/test.parquet", filesystem=gcs)

# %%
read_table(f"{SettingsGCS().name_bucket}/test.parquet", filesystem=gcs)

# %%
repo.name_file_last_modifieds

# %%
