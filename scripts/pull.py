# pull NYISO monthly zips

from logging import INFO, basicConfig
from sys import stdout

from nysmix.data import All
from nysmix.repository import RepositoryGCS

basicConfig(stream=stdout, level=INFO)

repo = RepositoryGCS()

All(repo).pull()
