"""
load csv data into a database
"""

from logging import INFO, basicConfig
from sys import stdout

from nysmix.data import All
from nysmix.db import DatabasePostgres
from nysmix.repository import RepositoryGCS

basicConfig(stream=stdout, level=INFO)

repo = RepositoryGCS()
db = DatabasePostgres()
db.create()


All(repo, db).load()
