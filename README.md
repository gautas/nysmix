# nysmix

NYS fuel mix from NYISO data https://gautas.gitlab.io/nysmix

## Environment variables
Add a file `.env` with these environment variables (google ones can be omitted if you are not using google cloud storage for files):
```sh
GOOGLE_APPLICATION_CREDENTIALS=/path/to/cred
GCS_NAME_BUCKET=name-bucket
GCS_NAME_PROJECT=name-project
POSTGRES_PASSWORD=mysecretpassword
POSTGRES_USER=postgres
POSTGRES_SERVER=db
POSTGRES_DB=postgres
POSTGRES_PORT=5432
```
